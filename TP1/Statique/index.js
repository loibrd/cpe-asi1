import { ApiCards } from "./core/api/api_cards.js";

function submitCreateForm(e) {
    e.preventDefault();
    const formData = new FormData(e.target);
    const formProps = Object.fromEntries(formData);
    // TODO: set un id en fonction des id déjà existants
    formProps['id'] = 19;
    formProps['userId'] = null;
    formProps['storeId'] = null;
    formProps['price'] = 100;
    formProps['smallImgUrl'] = formProps['imgUrl'];
    console.log(formProps);
    ApiCards.createCard(formProps);
}

window.onload = function () {
    document.createform.addEventListener("submit", submitCreateForm);
}
