class Card {
    constructor(id, name, description, family, affinity, imgUrl, smallImgUrl, energy, hp, defense, attack, price, userId, storeId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.family = family;
        this.affinity = affinity;;
        this.imgUrl = imgUrl;
        this.smallImgUrl = smallImgUrl;
        this.energy = energy;
        this.hp = hp;
        this.defense = defense;
        this.attack = attack;
        this.price = price;
        this.userId = userId;
        this.storeId = storeId;
    }

    static from(json) {
        return Object.assign(new Card(), json);
    }
}   
