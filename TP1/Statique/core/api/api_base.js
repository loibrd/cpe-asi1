async function get(req) {
    let params = {
        method: 'GET',
        mode: 'cors',
        cache: 'default'
    };
    let res = await fetch(req, params)
        .then((response) => response.json())
        .then((data) => {
            return data;
        });
    return res;
}
async function post(req, data) {
    let params = {
        method: 'POST',
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify(data),
    };
    let res = await fetch(req, params)
        .then((response) => response.json())
        .then((data) => {
            return data;
        });
    return res;
}

export {
    get,
    post
};
